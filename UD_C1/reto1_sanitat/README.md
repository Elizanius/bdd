## Introducción

Link de los archivos: <https://gitlab.com/Elizanius/bdd/-/tree/main/UD_C1/reto1_sanitat?ref_type=heads>

## Consulta 1

`SELECT HOSPITAL_COD , NOM , TELEFON FROM HOSPITAL;`

En esta consulta con el "SELECT" me muestra los registros de los atributos "HOSPITAL_COD , NOM y TELEFON " e idicamos con el "FROM" que esos atributos son de la tabla "HOSPITAL".

## Consulta 2

`SELECT HOSPITAL_COD , NOM , TELEFON FROM HOSPITAL WHERE NOM LIKE '_A%'`

El principio es el mismo que la primera consulta pero añadimos el "Where" para indicar que la petición va a ser al siguiente atributo que pongamos "En este caso a NOM", en este caso la petición que le hago es mediante la función "LIKE" le indico que quiero que me muestre todos los Hospitales que tienen una letra A en el segundo lugar del nombre.

## Consulta 3

`SELECT P.HOSPITAL_COD , SALA_COD , EMPLEAT_NO , COGNOM FROM PLANTILLA AS P;`

En esta consulta con el "SELECT" me muestra los registros de los atributos "P.HOSPITAL_COD , SALA_COD , EMPLEAT_NO y COGNOM " e idicamos con el "FROM" que esos atributos son de la tabla "PLATILLA" (Utilizo el "AS" para darle un alias a PLANTILLA).

## Consulta 4

`SELECT P.HOSPITAL_COD , SALA_COD , EMPLEAT_NO , COGNOM FROM PLANTILLA AS P WHERE TORN != 'N';`

El inicio de la consulta es el mismo que el de la consulta tres pero esta vez le añado la función "WHERE" para indicar que me filtre todos cuyo atributo "TORN" no contenga el valor "N" (Que significa noche).

## Consulta 5

`SELECT * FROM MALALT WHERE YEAR(DATA_NAIX) = 1960;`

Selecciona todos los datos de la tabla "MALALT" (indico que son todos mediante el asterisco "*") cuyo fecha de nacimiento sea 1960, esto ultimo lo hago indicando mediante la función "WHERE" y ayudandome de la función "YEAR"

## Consulta 6

`SELECT * FROM MALALT WHERE YEAR(DATA_NAIX) > 1960;`

Es exactamente lo mismo que la consulta 5 pero para indicar que sean nacidos despues del 1960 cambiamos el igual (=) por el mayor que (>).
