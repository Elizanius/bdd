# Consultas básicas II

## EMPRESA

Siempre utilizo el `USE empresa;` para indicar que la base sobre la que actuo es empresa.

### Query 1

Muestre los productos (código y descripción) que comercializa la empresa.

`SELECT PRODUCTE.* FROM PRODUCTE;`

### Query 2

Muestre los productos (código y descripción) que contienen la palabra tennis en la descripción.

`SELECT * FROM PRODUCTE WHERE PRODUCTE.DESCRIPCIO LIKE "%TENNIS%";`

### Query 3

Muestre el código, nombre, área y teléfono de los clientes de la empresa.

`SELECT C.CLIENT_COD, C.NOM, C.AREA, C.TELEFON FROM CLIENT AS C;`

### Query 4

Muestre los clientes (código, nombre, ciudad) que no son del área telefónica 636.

`SELECT C.CLIENT_COD, C.NOM, C.AREA, C.CIUTAT, C.AREA FROM CLIENT AS C WHERE C.AREA != 636;`

### Query 5

Muestre las órdenes de compra de la tabla de pedidos (código, fechas de orden y de envío).

`SELECT C.COM_NUM, C.COM_DATA, C.DATA_TRAMESA FROM COMANDA AS C;`

## VIDEOCLUB

A partir de aquí siempre utilizo el `USE videoclub;` para indicar que la base sobre la que actuo es videoclub.

## Query 6

Lista de nombres y teléfonos de los clientes.

`SELECT C.Nom, C.telefon FROM videoclub.CLIENT AS C;`

## Query 7

Lista de fechas e importes de las facturas.

`SELECT FACTURA.Data, FACTURA.Import FROM FACTURA;`

## Query 8

Lista de productos (descripción) facturados en la factura número 3.

`SELECT D.Descripcio FROM DETALLFACTURA AS D WHERE D.CodiFactura = 3;`

## Query 9

Lista de facturas ordenada de forma decreciente por importe.

`SELECT FACTURA.* FROM FACTURA ORDER BY FACTURA.Import DESC;`

## Query 10

Lista de los actores cuyo nombre comience por X.

`SELECT ACTOR.* FROM ACTOR WHERE ACTOR.Nom LIKE 'X%';`
